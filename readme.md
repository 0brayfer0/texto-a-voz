# Evol TTS

![](https://d2ujflorbtfzji.cloudfront.net/key-image/9ea7cfe2-5e34-46b6-9021-303b082bfb8c.png)

Texto a voz.

  - Modelo de aprendizaje profundo
  - Modelo Secuencia a Secuencia (Seq2seq)
  - Convoluciones para la extracciÃ³n de caracterÃ­sticas


### Recursos

EstÃ¡ basado en el modelo DC-TTS

* [DC-TTS](https://github.com/Kyubyong/dc_tts) - A TensorFlow Implementation of DC-TTS: yet another text-to-speech model


### Installation
[![N|Solid](http://www.infosciencelabs.com/img/exp/anaconda_logo.jpg)](https://www.anaconda.com/download/)
Se requiere tener instalado Anacoda con Python 3.6

Instalar dependencias con pip o conda
Sin GPU
```cmd
> conda install -c conda-forge tensorflow
> pip install tf-nightly
```

Con GPU

```cmd
> conda install -c anaconda tensorflow-gpu
> pip install tf-nightly-gpu
```

Para escuchar el audio en el notebook
```cmd
> pip install sounddevice
```



### Tareas

 - Optimizar el tiempo de respuesta de la inferencia del modelo
 - Agregar otros idiomas